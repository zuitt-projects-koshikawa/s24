db.products.insertMany([

		{
			"name" : "Iphone X",
			"price" : 30000,
			"isActive" : true

		},
		{
			"name" : "Samsung Galaxy S21",
			"price" : 51000,
			"isActive" : true

		},
		{
			"name" : "Razer Blackshark V2X",
			"price" : 2800,
			"isActive" : false

		},
		{
			"name" : "Rakk Gaming Mouse",
			"price" : 1800,
			"isActive" : true

		},
		{
			"name" : "Razer Mechanical Keyboard",
			"price" : 4000,
			"isActive" : true

		}

	])

db.users.insertMany([
		{
			"firstName" : "Mary Jane",
			"lastName" : "Watson",
			"email" : "mjtiger@gmail.com",
			"password" : "tigerjackpot15",
			"isAdmin" : false,
		},
		{
			"firstName" : "Gwen",
			"lastName" : "Stacy",
			"email" : "stacyTech@gmail.com",
			"password" : "stacyTech1991",
			"isAdmin" : true,
		},{
			"firstName" : "Peter",
			"lastName" : "Parker",
			"email" : "peterWebDev@gmail.com",
			"password" : "webDeveloperPeter",
			"isAdmin" : true,
		},{
			"firstName" : "Jonah",
			"lastName" : "Jameson",
			"email" : "jjjameson@gmail.com",
			"password" : "spideyisamenace",
			"isAdmin" : false,
		},{
			"firstName" : "Otto",
			"lastName" : "Octavius",
			"email" : "ottoOctopi@gmail.com",
			"password" : "docOck15",
			"isAdmin" : true,
		}

	])

db.users.find({
	$or: [
		{"firstName": {$regex : 'y', $options : '$i'}},
		{"lastName"	: {$regex : 'y', $options : '$i'}}
	]
},	
{
	"_id" : 0, "email" : 1, "isAdmin" : 1
})


db.users.find({
	$and: [
		{"firstName" : {$regex : 'e', $options : '$i'}},
		{"isAdmin" : true}
	]
},
{
	"_id" : 0, "email" : 1, "isAdmin" : 1
})

db.products.find({
	$and: [
		{"name" : {$regex : 'x', $options : '$i'}},
		{"price" : {$gte : 50000}}
	]
})

db.products.updateMany(
	{"price" : {$lt : 2000}},
	{$set : {"isActive" : false}}
)

db.products.updateMany(
	{"price" : {$gt : 20000}},
	{"price"}
)

db.products.deleteMany({"price" : {$gt: 20000}})